﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICTPRG527ASS1
{
    [Serializable]
    public class Library
    {
        public Library()
        {
            //nothing yet
        }

        //copy constructor
        public Library(Library library)
        {

        }

        private List<Videogame> videoLibrary = new List<Videogame>();

        public List<Videogame> VideoLibrary
        {
            get { return videoLibrary; }
            set { videoLibrary = value; }
        }
    }
}
