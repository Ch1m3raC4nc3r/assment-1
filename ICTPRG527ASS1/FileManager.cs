﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICTPRG527ASS1
{
    class FileManager
    {
        static String fileName = "data.bin";
        static String filePath = System.IO.Directory.GetCurrentDirectory();
        static String fileAndPath = System.IO.Path.Combine(filePath, fileName);

        static public void SaveData()
        {
            //serialize the data
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(fileAndPath, FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, Globals.library);
            stream.Close();
        }
    }
}
