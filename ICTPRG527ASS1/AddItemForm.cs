﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ICTPRG527ASS1
{
    public partial class AddItemForm : Form
    {
        public object Messagebox { get; private set; }

        public AddItemForm()
        {
            InitializeComponent();
            ((Control)pictureBox1).AllowDrop = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //create object
            Videogame vgame = new Videogame();
            vgame.Title = titleTextBox.Text;
            vgame.Genre = genreTextBox.Text;
            vgame.Publisher = publisherTextBox.Text;
            vgame.Developer = developerTextBox.Text;
            vgame.ReleaseDate = releaseDateTimePicker.Value;


            if (pictureBox1.Image != null)
            {
                try
                {
                    pictureBox1.Image.Tag = vgame.Title;
                    vgame.Img = pictureBox1.Image.Tag.ToString();
                    //save the image somewhere
                    pictureBox1.Image.Save(@"artwork\\" + vgame.Img + ".png", ImageFormat.Png);
                    //save the data
                    FileManager.SaveData();
                }

                catch (Exception)
                {
                    //bad things happened
                    MessageBox.Show("an error occured while attempting to save the file.");
                    pictureBox1_Click(sender, e);
                }
            }
            else
            {
                //
                pictureBox1_Click(sender, e);
            }
         }
            //Console.WriteLine(vgame.ToString());

            
        

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void pictureBox1_DragDrop(object sender, DragEventArgs e)
        {
            String[] files = (String[])e.Data.GetData(DataFormats.FileDrop);
            //assign the image
            pictureBox1.Image = Image.FromFile(files[0]);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            //open dialougge whre we can pick a file.
            using (OpenFileDialog ofd = new OpenFileDialog())

            {
                ofd.Title = "choose an image.";
                ofd.Filter = "image files (*.bmp;*.jpg;*.jpeg;*.png;) | *.BMP;*.JPG;*.JPEG;*.PNG";
                ofd.InitialDirectory = "Pictures";

                //open the file picker dialoug
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    //if the user hits ok, aasign the image.
                    pictureBox1.Image = new Bitmap(ofd.FileName);
                }
            }
        }
    }
}
