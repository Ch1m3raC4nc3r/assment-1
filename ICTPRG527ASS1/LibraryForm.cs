﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ICTPRG527ASS1
{
    public partial class LibraryForm : Form
    {
        public LibraryForm()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Remove_Load(object sender, EventArgs e)
        {

        }

        private void addToLibraryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //create an AddItemForm object, then open it
            AddItemForm addForm = new AddItemForm();
            addForm.Show();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 about = new AboutBox1();
            about.Show();
        }

        private void gettingStartedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Help.ShowHelp(this, @"help\index.html");
        }
    }
}
